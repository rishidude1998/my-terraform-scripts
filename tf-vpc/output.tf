output "vpc_id" {
  value = "${aws_vpc.vpc_name.id}"
}

output "vpc_public_sn1_id" {
  value = "${aws_subnet.vpc_public_sn1.id}"
}

output "vpc_private_sn1_id" {
  value = "${aws_subnet.vpc_private_sn1.id}"
}

output "vpc_public_sn2_id" {
  value = "${aws_subnet.vpc_public_sn2.id}"
}
output "Mongo-Bastion_public_ip" {
  value       = aws_instance.Mongo-bastion.public_ip
}
output "Mongo-Master_private_ip" {
  value       = aws_instance.Mongo-master.private_ip
}
output "Mongo-Slave1_private_ip" {
  value       = aws_instance.Mongo-slave-1.private_ip
}
output "Mongo-Slave2_private_ip" {
  value       = aws_instance.Mongo-slave-2.private_ip
}