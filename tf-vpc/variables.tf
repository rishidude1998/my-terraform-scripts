variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {}

variable "office_cidr" {
    default = ["15.206.192.103/32","202.164.46.160/29", "14.98.185.24/30","35.154.39.27/32"]
}

variable "vpc_cidr_block" {
  description = "Addressing for demo Network"
  default = "10.0.0.0/16"
}

variable "project_name" {
  description = "Project Name"
  default = "Demo"
}

variable "vpc_public_subnet_1_cidr" {
  description = "Public CIDR for public subnet 1"
  default = "10.0.1.0/24"
}

variable "vpc_public_subnet_1_availability_zone" {
  description = "Availability zone  for public subnet 1"
  default = "ap-south-1a"
}

variable "vpc_public_subnet_2_cidr" {
  description = "Public CIDR for public subnet 2"
  default = "10.0.2.0/24"
}

variable "vpc_public_subnet_2_availability_zone" {
  description = "Availability zone for public subnet 2"
  default = "ap-south-1b"
}

variable "vpc_public_subnet_3_cidr" {
  description = "Public CIDR for public subnet 3"
  default = "10.0.3.0/24"
}

variable "vpc_public_subnet_3_availability_zone" {
  description = "Availability zone for public subnet 3"
  default = "ap-south-1a"
}

variable "vpc_private_subnet_1_cidr" {
  description = "CIDR for private subnet 1"
  default = "10.0.4.0/24"
}

variable "vpc_private_subnet_1_availability_zone" {
  description = "Availability zone for private subnet 1"
  default = "ap-south-1a"
}

variable "vpc_private_subnet_2_cidr" {
  description = "CIDR for private subnet 2"
  default = "10.0.5.0/24"
}

variable "vpc_private_subnet_2_availability_zone" {
  description = "Availability zone for private subnet 2"
  default = "ap-south-1b"
}

variable "vpc_private_subnet_3_cidr" {
  description = "CIDR for private subnet 3"
  default = "10.0.6.0/24"
}

variable "vpc_private_subnet_3_availability_zone" {
  description = "Availability zone  for private subnet 3"
  default = "ap-south-1a"
}

variable "AMIS" {
  type = map(string)
  default = {
    us-east-1      = "ami-467ca739"
    us-east-2      = "ami-976152f2"
    us-west-1      = "ami-46e1f226"
    us-west-2      = "ami-6b8cef13"
    eu-west-1      = "ami-24506250"
    sa-east-1      = "ami-3e3be423"
    ap-south-1     = "ami-b46f48db"
    ap-northeast-1 = "ami-28ddc154"
    ap-northeast-2 = "ami-efaf0181"
    ap-southeast-1 = "ami-64260718"
    ap-southeast-2 = "ami-60a26a02"
    ap-southeast-2 = "ami-60a26a02"
    ca-central-1   = "ami-2f39bf4b"
    eu-central-1   = "ami-1b316af0"
    eu-west-1      = "ami-9cbe9be5"
    eu-west-2      = "ami-c12dcda6"
    eu-west-2      = "ami-c12dcda6"
    eu-west-3      = "ami-cae150b7"
    sa-east-1      = "ami-f09dcc9c"
  }
}

variable "mongo_master_instance_type" {
  default = "t2.small"
}

variable "mongo_slave_instance_type" {
  default = "t2.small"
}

variable "mongo_master_instance_key_name" {
  description = "Key name for Mongo Master instance"
  default= "master"
}

variable "mongo_slave_instance_key_name" {
  description = "Key name for Mongo Slave instance"
  default= "slave"
}
variable "PATH_TO_PRIVATE_KEY" {

}

variable "PATH_TO_PUBLIC_KEY" {

}


