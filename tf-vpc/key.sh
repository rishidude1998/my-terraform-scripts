if [[ $1 ]]
then
    ssh-keygen -t rsa -f ./$1 -b 2048  -C "$1" -q -N ""
else
    echo 'Usage: ./keygen.sh filename-for-key comment-for-key';
fi