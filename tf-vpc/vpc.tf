# Define a vpc
resource "aws_vpc" "vpc_name" {
  cidr_block = var.vpc_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
  Name = "${var.project_name}_vpc"
}
  
}


# Public subnets
resource "aws_subnet" "vpc_public_sn1" {
  vpc_id = aws_vpc.vpc_name.id
  cidr_block = var.vpc_public_subnet_1_cidr
  availability_zone = var.vpc_public_subnet_1_availability_zone
  tags = {
  Name = "${var.project_name}_public_sn1"
}
}

resource "aws_subnet" "vpc_public_sn2" {
  vpc_id = aws_vpc.vpc_name.id
  cidr_block = var.vpc_public_subnet_2_cidr
  availability_zone = var.vpc_public_subnet_2_availability_zone
  tags = {
  Name = "${var.project_name}_public_sn2"
}
}

resource "aws_subnet" "vpc_public_sn3" {
  vpc_id = aws_vpc.vpc_name.id
  cidr_block = var.vpc_public_subnet_3_cidr
  availability_zone = var.vpc_public_subnet_3_availability_zone
  tags = {
  Name = "${var.project_name}_public_sn3"
}
}

# Private subnets
resource "aws_subnet" "vpc_private_sn1" {
  vpc_id = aws_vpc.vpc_name.id
  cidr_block = var.vpc_private_subnet_1_cidr
  availability_zone = var.vpc_private_subnet_1_availability_zone
  tags = {
  Name = "${var.project_name}_private_sn1"
}
}

resource "aws_subnet" "vpc_private_sn2" {
  vpc_id = aws_vpc.vpc_name.id
  cidr_block = var.vpc_private_subnet_2_cidr
  availability_zone = var.vpc_private_subnet_2_availability_zone
  tags = {
  Name = "${var.project_name}_private_sn2"
}
}

resource "aws_subnet" "vpc_private_sn3" {
  vpc_id = aws_vpc.vpc_name.id
  cidr_block = var.vpc_private_subnet_3_cidr
  availability_zone = var.vpc_private_subnet_3_availability_zone
  tags = {
  Name = "${var.project_name}_private_sn3"
}
}

# Internet gateway for the public subnet
resource "aws_internet_gateway" "demo_ig" {
  vpc_id = aws_vpc.vpc_name.id
  tags = {
  Name = "${var.project_name}_demo_ig"
}
}

#NAT Gateway
resource "aws_eip" "nat_elastic_ip" {
  vpc = true
  tags = {
    Name = "${var.project_name}_nat_elastic_ip"
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_elastic_ip.id
  subnet_id     = aws_subnet.vpc_public_sn1.id

  tags = {
    Name = "${var.project_name}_nat_gateway"
  }
}

# Routing table for public subnet
resource "aws_route_table" "vpc_public_sn_rt" {
  vpc_id = aws_vpc.vpc_name.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo_ig.id
  }
  tags = {
  Name = "${var.project_name}_vpc_public_sn_rt"
}
}

# Associate the routing table to public subnets
resource "aws_route_table_association" "vpc_public_sn_rt_assn_1" {
  subnet_id = "${aws_subnet.vpc_public_sn1.id}"
  route_table_id = "${aws_route_table.vpc_public_sn_rt.id}"
}

resource "aws_route_table_association" "vpc_public_sn_rt_assn_2" {
  subnet_id = "${aws_subnet.vpc_public_sn2.id}"
  route_table_id = "${aws_route_table.vpc_public_sn_rt.id}"
}

resource "aws_route_table_association" "vpc_public_sn_rt_assn_3" {
  subnet_id = "${aws_subnet.vpc_public_sn3.id}"
  route_table_id = "${aws_route_table.vpc_public_sn_rt.id}"
}

# Routing table for private subnets
resource "aws_route_table" "vpc_private_sn_rt" {
  vpc_id = aws_vpc.vpc_name.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
  tags = {
  Name = "${var.project_name}_vpc_private_sn_rt"
}
}

# Associate the routing table to private subnets
resource "aws_route_table_association" "vpc_private_sn_rt_assn_1" {
  subnet_id = "${aws_subnet.vpc_private_sn1.id}"
  route_table_id = "${aws_route_table.vpc_private_sn_rt.id}"
}

resource "aws_route_table_association" "vpc_private_sn_rt_assn_2" {
  subnet_id = "${aws_subnet.vpc_private_sn2.id}"
  route_table_id = "${aws_route_table.vpc_private_sn_rt.id}"
}

resource "aws_route_table_association" "vpc_private_sn_rt_assn_3" {
  subnet_id = "${aws_subnet.vpc_private_sn3.id}"
  route_table_id = "${aws_route_table.vpc_private_sn_rt.id}"
}

